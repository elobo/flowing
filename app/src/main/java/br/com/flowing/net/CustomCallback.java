package br.com.flowing.net;

import android.content.Context;

import java.lang.ref.WeakReference;

import br.com.flowing.R;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by edgard on 23/05/16.
 */
public abstract class CustomCallback<T> implements Callback<T> {

    private WeakReference<Context> weakContext;

    protected CustomCallback(Context context) {
        this.weakContext = new WeakReference<Context>(context);
    }

    @Override
    public void success(T t, retrofit.client.Response response) {
        switchVisibility();
        callSuccess(t, response);
    }

    @Override
    public void failure(RetrofitError error) {
        ErrorResponse response = null;

        String msg;
        String shortMsg;
        Context context = null;
        if( weakContext != null ){
            context = weakContext.get();
        }

        switch (error.getKind()){
            case NETWORK:
                msg = "Ops! Aconteceu um problema com a conexão. Tente novamente mais tarde";
                shortMsg = "Ops! Aconteceu um problema com a conexão";

                if (context != null) {
                    msg = context.getString(R.string.network_error);
                    shortMsg = msg;
                }

                response = new ErrorResponse(msg, shortMsg);
                break;
            case HTTP:
            case UNEXPECTED:
            case CONVERSION:
                msg = "Ops! Aconteceu algo inesperado. Estamos trabalhando para resolver o mais rápido possível";
                shortMsg = "Ops! Aconteceu algo inesperado";

                if (context != null) {
                    msg = context.getString(R.string.unexpected_error);
                    shortMsg = msg;
                }

                response = new ErrorResponse(msg, shortMsg);
                if(error.getResponse() != null ) {
                    response.setStatusCode(error.getResponse().getStatus());
                }
                break;
        }

        switchVisibility();
        response.setKind(error.getKind());
        callError(response);
    }

    public void callSuccess(T t, Response response){
        onSuccess(t, response);
    }

    public void callError(ErrorResponse response){
        onError(response);
    }
    public abstract void onError(ErrorResponse response);
    public abstract void onSuccess(T t, retrofit.client.Response response);


    public void switchVisibility(){
    }

    public class ErrorResponse{
        private String message;
        private String shortMessage;
        private int statusCode;
        private RetrofitError.Kind kind;

        public ErrorResponse(String message, String shortMessage) {
            this.message = message;
            this.shortMessage = shortMessage;
        }

        public RetrofitError.Kind getKind() {
            return kind;
        }

        public void setKind(RetrofitError.Kind kind) {
            this.kind = kind;
        }

        public String getMessage() {
            return message;
        }

        public String getShortMessage() {
            return shortMessage;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }
    }
}

