package br.com.flowing.net;

import java.util.List;
import java.util.Objects;

import br.com.flowing.model.Coach;
import br.com.flowing.model.Program;
import br.com.flowing.model.User;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by edgard on 23/05/16.
 */
public interface UserServices {

    @FormUrlEncoded
    @POST(RestServices.LOGIN)
    void login(@Field("grant_type") String grantType, @Field("username") String email,
               @Field("password") String password, CustomCallback<User> callback);

    @GET(RestServices.AVATARS)
    void avatars(CustomCallback<List<Coach>> callback);

    @GET(RestServices.PROGRAMS)
    void programs(CustomCallback<List<Program>> callback);
}
