package br.com.flowing.net;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.client.UrlConnectionClient;

/**
 * Created by edgard on 23/05/16.
 */
public class FlowingClient extends UrlConnectionClient {

    protected HttpURLConnection openConnection(Request request) throws IOException {
        HttpURLConnection connection =
                (HttpURLConnection) new URL(request.getUrl()).openConnection();
        connection.setConnectTimeout(RestServices.CONNECT_TIMEOUT);
        connection.setReadTimeout(RestServices.READ_TIMEOUT);
        connection.setRequestProperty("Connection", "close");
        return connection;
    }

    @Override
    public Response execute(Request request) throws IOException {
        return super.execute(request);
    }
}
