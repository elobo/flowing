package br.com.flowing.net;

import android.content.Context;
import android.util.Log;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by edgard on 23/05/16.
 */
public class RestServices {
    public static final int CONNECT_TIMEOUT = 5000;
    public static final int READ_TIMEOUT = 30000;
    public static final String ROOT_URL = "http://dev.flowing.com.br";
    public static final String LOGIN = "/account/gettoken";
    public static final String AVATARS = "/v1/configurations/avatars";
    public static final String PROGRAMS = "/v1/users/recomendedprograms";
    private static RestAdapter restAdapter;

    private static RestAdapter getAdapter(Context context, String url, String acessToken) {
        //if( restAdapter == null ){
            restAdapter = buildAdapter(context, url, acessToken);
        //}
        return restAdapter;
    }

    private static <T> T getServices(Context applicationContext, Class<T> clazz, String acessToken){
        return getAdapter(applicationContext.getApplicationContext(), ROOT_URL, acessToken).create(clazz);
    }

    public static UserServices getUserServices(Context applicationContext, String acessToken){
        return getServices(applicationContext, UserServices.class, acessToken);
    }

    private static RestAdapter buildAdapter(Context context, String url, String accessToken) {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonWithExposeHttpMessageConverter())
                .setEndpoint(url)
                .setClient(new FlowingClient());
        if(accessToken != null) {
            builder = builder.setRequestInterceptor(new RetrofitAuthenticatedInterceptor(accessToken));
        } else{
            builder = builder.setRequestInterceptor(new RetrofitInterceptor());
        }

        return builder.build();
    }

    private static class RetrofitInterceptor implements RequestInterceptor {
        @Override
        public void intercept(RequestFacade requestFacade) {
            requestFacade.addHeader("Cache-Control", "no-cache");
            requestFacade.addHeader("charset", "utf-8");
        }
    }

    private static class RetrofitAuthenticatedInterceptor extends RetrofitInterceptor {

        private String accessToken;

        private RetrofitAuthenticatedInterceptor(String accessToken) {
            this.accessToken = accessToken;
        }
        @Override
        public void intercept(RequestInterceptor.RequestFacade requestFacade) {
            super.intercept(requestFacade);
            requestFacade.addHeader("Authorization", "bearer " + accessToken);
        }
    }

}
