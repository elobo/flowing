package br.com.flowing.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by edgard on 24/05/16.
 */
public class Program implements Parcelable {

    @Expose
    @SerializedName("SignedIn")
    private boolean signedIn;
    @Expose
    @SerializedName("RecomendationRate")
    private double recomendationRate;
    @Expose
    @SerializedName("TotalParticipants")
    private int totalParticipants;
    @Expose
    @SerializedName("ProgramId")
    private int programId;
    @Expose
    @SerializedName("UserId")
    private int userId;
    @Expose
    @SerializedName("ProgramTypeId")
    private int programTypeId;
    @Expose
    @SerializedName("PartnerDescription")
    private String partnerDescription;
    @Expose
    @SerializedName("PartnerProfilePicture")
    private String partnerProfilePicture;
    @Expose
    @SerializedName("PartnerSignature")
    private String partnerSignature;
    @Expose
    @SerializedName("PartnerName")
    private String partnerName;
    @Expose
    @SerializedName("ProgramRetinaImage_wide")
    private String programRetinaImageWide;
    @Expose
    @SerializedName("ProgramImage_wide")
    private String programImageWide;
    @Expose
    @SerializedName("ProgramRetinaImage_square")
    private String programRetinaImageSquare;
    @Expose
    @SerializedName("ProgramImage_square")
    private String programImageSquare;
    @Expose
    @SerializedName("ProgramShortDesciption")
    private String programShortDesciption;
    @Expose
    @SerializedName("ProgramDescription")
    private String programDescription;
    @Expose
    @SerializedName("ProgramType")
    private String programType;
    @Expose
    @SerializedName("ProgramName")
    private String programName;
    @Expose
    @SerializedName("UserName")
    private String userName;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isSignedIn() {
        return signedIn;
    }

    public void setSignedIn(boolean signedIn) {
        this.signedIn = signedIn;
    }

    public double getRecomendationRate() {
        return recomendationRate;
    }

    public void setRecomendationRate(double recomendationRate) {
        this.recomendationRate = recomendationRate;
    }

    public int getTotalParticipants() {
        return totalParticipants;
    }

    public void setTotalParticipants(int totalParticipants) {
        this.totalParticipants = totalParticipants;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getProgramTypeId() {
        return programTypeId;
    }

    public void setProgramTypeId(int programTypeId) {
        this.programTypeId = programTypeId;
    }

    public String getPartnerDescription() {
        return partnerDescription;
    }

    public void setPartnerDescription(String partnerDescription) {
        this.partnerDescription = partnerDescription;
    }

    public String getPartnerProfilePicture() {
        return partnerProfilePicture;
    }

    public void setPartnerProfilePicture(String partnerProfilePicture) {
        this.partnerProfilePicture = partnerProfilePicture;
    }

    public String getPartnerSignature() {
        return partnerSignature;
    }

    public void setPartnerSignature(String partnerSignature) {
        this.partnerSignature = partnerSignature;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getProgramRetinaImageWide() {
        return programRetinaImageWide;
    }

    public void setProgramRetinaImageWide(String programRetinaImageWide) {
        this.programRetinaImageWide = programRetinaImageWide;
    }

    public String getProgramImageWide() {
        return programImageWide;
    }

    public void setProgramImageWide(String programImageWide) {
        this.programImageWide = programImageWide;
    }

    public String getProgramRetinaImageSquare() {
        return programRetinaImageSquare;
    }

    public void setProgramRetinaImageSquare(String programRetinaImageSquare) {
        this.programRetinaImageSquare = programRetinaImageSquare;
    }

    public String getProgramImageSquare() {
        return programImageSquare;
    }

    public void setProgramImageSquare(String programImageSquare) {
        this.programImageSquare = programImageSquare;
    }

    public String getProgramShortDesciption() {
        return programShortDesciption;
    }

    public void setProgramShortDesciption(String programShortDesciption) {
        this.programShortDesciption = programShortDesciption;
    }

    public String getProgramDescription() {
        return programDescription;
    }

    public void setProgramDescription(String programDescription) {
        this.programDescription = programDescription;
    }

    public String getProgramType() {
        return programType;
    }

    public void setProgramType(String programType) {
        this.programType = programType;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.signedIn ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.recomendationRate);
        dest.writeInt(this.totalParticipants);
        dest.writeInt(this.programId);
        dest.writeInt(this.userId);
        dest.writeInt(this.programTypeId);
        dest.writeString(this.partnerDescription);
        dest.writeString(this.partnerProfilePicture);
        dest.writeString(this.partnerSignature);
        dest.writeString(this.partnerName);
        dest.writeString(this.programRetinaImageWide);
        dest.writeString(this.programImageWide);
        dest.writeString(this.programRetinaImageSquare);
        dest.writeString(this.programImageSquare);
        dest.writeString(this.programShortDesciption);
        dest.writeString(this.programDescription);
        dest.writeString(this.programType);
        dest.writeString(this.programName);
        dest.writeString(this.userName);
    }

    public Program() {
    }

    protected Program(Parcel in) {
        this.signedIn = in.readByte() != 0;
        this.recomendationRate = in.readDouble();
        this.totalParticipants = in.readInt();
        this.programId = in.readInt();
        this.userId = in.readInt();
        this.programTypeId = in.readInt();
        this.partnerDescription = in.readString();
        this.partnerProfilePicture = in.readString();
        this.partnerSignature = in.readString();
        this.partnerName = in.readString();
        this.programRetinaImageWide = in.readString();
        this.programImageWide = in.readString();
        this.programRetinaImageSquare = in.readString();
        this.programImageSquare = in.readString();
        this.programShortDesciption = in.readString();
        this.programDescription = in.readString();
        this.programType = in.readString();
        this.programName = in.readString();
        this.userName = in.readString();
    }

    public static final Parcelable.Creator<Program> CREATOR = new Parcelable.Creator<Program>() {
        @Override
        public Program createFromParcel(Parcel source) {
            return new Program(source);
        }

        @Override
        public Program[] newArray(int size) {
            return new Program[size];
        }
    };
}
