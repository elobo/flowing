package br.com.flowing.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by edgard on 24/05/16.
 */
public class Coach implements Parcelable {

    @Expose
    @SerializedName("IsSelected")
    private boolean isSelected;
    @Expose
    @SerializedName("AvatarId")
    private long avatarId;
    @Expose
    @SerializedName("AvatarPicturePath")
    private String avatarPicturePath;
    @Expose
    @SerializedName("AvatarBio")
    private String avatarBio;
    @Expose
    @SerializedName("AvatarName")
    private String avatarName;


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public long getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(long avatarId) {
        this.avatarId = avatarId;
    }

    public String getAvatarPicturePath() {
        return avatarPicturePath;
    }

    public void setAvatarPicturePath(String avatarPicturePath) {
        this.avatarPicturePath = avatarPicturePath;
    }

    public String getAvatarBio() {
        return avatarBio;
    }

    public void setAvatarBio(String avatarBio) {
        this.avatarBio = avatarBio;
    }

    public String getAvatarName() {
        return avatarName;
    }

    public void setAvatarName(String avatarName) {
        this.avatarName = avatarName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
        dest.writeLong(this.avatarId);
        dest.writeString(this.avatarPicturePath);
        dest.writeString(this.avatarBio);
        dest.writeString(this.avatarName);
    }

    public Coach() {
    }

    protected Coach(Parcel in) {
        this.isSelected = in.readByte() != 0;
        this.avatarId = in.readLong();
        this.avatarPicturePath = in.readString();
        this.avatarBio = in.readString();
        this.avatarName = in.readString();
    }

    public static final Parcelable.Creator<Coach> CREATOR = new Parcelable.Creator<Coach>() {
        @Override
        public Coach createFromParcel(Parcel source) {
            return new Coach(source);
        }
        @Override
        public Coach[] newArray(int size) {
            return new Coach[size];
        }
    };
}
