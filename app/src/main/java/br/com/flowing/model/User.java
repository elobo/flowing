package br.com.flowing.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by edgard on 24/05/16.
 */
public class User {

    @Expose
    @SerializedName("access_token")
    private String acessToken;
    @Expose
    @SerializedName("FullName")
    private String fullName;
    @Expose
    @SerializedName("Company")
    private String company;
    @Expose
    @SerializedName("CompanyLogoPath")
    private String companyLogoPath;


    public String getAcessToken() {
        return acessToken;
    }

    public void setAcessToken(String acessToken) {
        this.acessToken = acessToken;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyLogoPath() {
        return companyLogoPath;
    }

    public void setCompanyLogoPath(String companyLogoPath) {
        this.companyLogoPath = companyLogoPath;
    }
}
