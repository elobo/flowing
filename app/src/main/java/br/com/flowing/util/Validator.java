package br.com.flowing.util;

import android.util.Patterns;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by edgard on 23/05/16.
 */
public class Validator {

    public static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static boolean field(String field, String validatePattern){
        Pattern pattern = Pattern.compile(validatePattern);
        Matcher matcher = pattern.matcher(field);
        return matcher.matches() && !field.isEmpty();
    }

}
