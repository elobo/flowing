package br.com.flowing.util;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by edgard on 23/05/16.
 */
public class UserPreferences {

    private static final String PREFERENCES_SESSION = "preferences_session";
    private static final String TOKEN = "user_token";
    private static final String HAS_USER = "has_user";

    private SharedPreferences prefs;
    private static UserPreferences instance;

    public UserPreferences(SharedPreferences sharedPreferences) {
        prefs = sharedPreferences;
    }

    public static UserPreferences getInstance(Context context){
        if( instance != null ){
            return instance;
        }
        return new UserPreferences(getSharedPreferences(context));
    }

    public static SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences(PREFERENCES_SESSION, Context.MODE_PRIVATE);
    }

    public void setUserToken(String userToken){
        prefs.edit().putString(TOKEN, userToken).commit();
    }

    public String userToken() {
        return prefs.getString(TOKEN, null);
    }

    public void saveHasUser(boolean hasUser){
        prefs.edit().putBoolean(HAS_USER, hasUser).commit();
    }

    public boolean hasUser() {
        return prefs.getBoolean(HAS_USER, false);
    }

    public void clean() {
        prefs.edit().clear().commit();
    }


}
