package br.com.flowing.ui.programs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import br.com.flowing.R;
import br.com.flowing.model.Coach;
import br.com.flowing.model.Program;
import br.com.flowing.net.CustomCallback;
import br.com.flowing.net.RestServices;
import br.com.flowing.util.UserPreferences;
import retrofit.client.Response;

/**
 * Created by edgard on 24/05/16.
 */
public class ProgramsFragment extends Fragment {

    private static final String PROGRAMS = "programs";
    private TextView emptyText;
    private RecyclerView programsRecycler;
    private ProgressBar progress;
    private List<Program> mPrograms;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    public static ProgramsFragment newInstance(){
        return new ProgramsFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inflateViews(view);
        configView();

        if(savedInstanceState != null ){
            mPrograms = savedInstanceState.getParcelableArrayList(PROGRAMS);
        }

        if(mPrograms == null) {
            showProgress();
            programs();
        } else {
            showPrograms(mPrograms);
        }
    }

    private void inflateViews(View view) {
        emptyText = (TextView) view.findViewById(R.id.emptyText);
        programsRecycler = (RecyclerView) view.findViewById(R.id.recycler);
        progress = (ProgressBar) view.findViewById(R.id.progress);
    }

    private void configView() {
        programsRecycler.setHasFixedSize(true);
        programsRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mPrograms != null) {
            outState.putParcelableArrayList(PROGRAMS, new ArrayList<Program>(mPrograms));
        }
    }

    private void showProgress(){
        progress.setVisibility(View.VISIBLE);
    }

    private void showPrograms(List<Program> programs) {
        if(isAdded()) {
            mPrograms = programs;
            programsRecycler.setVisibility(View.VISIBLE);
            emptyText.setVisibility(View.GONE);
            progress.setVisibility(View.GONE);
            updateAdapter(programs);
        }
    }

    private void showEmpty() {
        if(isAdded()) {
            programsRecycler.setVisibility(View.GONE);
            progress.setVisibility(View.GONE);
            emptyText.setVisibility(View.VISIBLE);
            emptyText.setText(R.string.empty_program);
        }
    }

    private void updateAdapter(List<Program> programs) {
        ProgramAdapter programAdapter = new ProgramAdapter(getContext(), programs);
        programsRecycler.setAdapter(programAdapter);
    }

    private void programs() {
        RestServices.getUserServices(getActivity(), UserPreferences.getInstance(getActivity().getApplicationContext()).userToken())
                .programs(new CustomCallback<List<Program>>(getActivity()) {
                    @Override
                    public void onError(ErrorResponse response) {
                        Toast.makeText(getActivity(), R.string.error_api_coach, Toast.LENGTH_LONG).show();
                        showEmpty();
                    }

                    @Override
                    public void onSuccess(List<Program> programs, Response response) {
                        if(programs != null && programs.size() > 0)
                            showPrograms(programs);
                        else
                            showEmpty();
                    }
                });
    }
}
