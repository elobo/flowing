package br.com.flowing.ui.programs;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.List;

import br.com.flowing.R;
import br.com.flowing.model.Coach;
import br.com.flowing.model.Program;
import br.com.flowing.util.CircleTransformation;

/**
 * Created by edgard on 24/05/16.
 */
public class ProgramAdapter extends RecyclerView.Adapter<ProgramAdapter.ViewHolder>{

    private List<Program> mPrograms;
    private int mDefaultViewSize;
    private Context mContext;

    public ProgramAdapter(Context context, List<Program> programs){
        mContext = context;
        mPrograms = programs;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mDefaultViewSize = (int) parent.getContext().getResources().getDimension(R.dimen.default_view_size);
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_program, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        uploadImageBackground(holder, mPrograms.get(position).getProgramImageWide());
        uploadImage(holder, mPrograms.get(position).getPartnerProfilePicture());
        holder.title.setText(mPrograms.get(position).getProgramName());
        //holder.title.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/source-sans-pro.extralight.ttf"));
        holder.description.setText(mPrograms.get(position).getProgramShortDesciption());
        //holder.description.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/source-sans-pro.extralight.ttf"));
        holder.name.setText(mPrograms.get(position).getPartnerName());
        //holder.name.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/source-sans-pro.extralight.tff"));
        holder.titulo.setText(mPrograms.get(position).getPartnerSignature());
        //holder.titulo.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/source-sans-pro.extralight.tff"));
    }

    private void uploadImageBackground(ViewHolder holder, String picture){
        try {
            if(picture != null && !picture.isEmpty()) {
                WindowManager windowManager = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
                int viewSizeWidth = windowManager.getDefaultDisplay().getWidth();
                int viewSizeHeight = windowManager.getDefaultDisplay().getHeight();
                Picasso.with(holder.background.getContext())
                        .load(picture)
                        .centerCrop()
                        .resize(viewSizeWidth, viewSizeHeight)
                        .into(holder.background, new PicassoCallback(holder));
            }
        } catch (Exception e) {
        }
    }

    private void uploadImage(ViewHolder holder, String picture){
        try {
            if(picture != null && !picture.isEmpty()) {
                Picasso.with(holder.picture.getContext())
                        .load(picture)
                        .centerCrop()
                        .transform(new CircleTransformation())
                        .resize(mDefaultViewSize, mDefaultViewSize)
                        .into(holder.picture, new PicassoCallbackBackground(holder));
            } else {
                loadDefaultPicture(holder);
            }
        } catch (Exception e) {
        }
    }

    private void loadDefaultPicture(ViewHolder holder){
        holder.picture.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        holder.picture.setBackgroundResource(R.drawable.picture_circle);
        holder.picture.setImageResource(R.drawable.ic_perm_identity_white_24dp);
    }

    @Override
    public int getItemCount() {
        return mPrograms.size();
    }

    class PicassoCallbackBackground implements com.squareup.picasso.Callback{
        private ViewHolder holder;

        public PicassoCallbackBackground(ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        public void onSuccess() {
            holder = null;
        }

        @Override
        public void onError() {
            holder = null;
        }
    }

    class PicassoCallback implements com.squareup.picasso.Callback{
        private ViewHolder holder;

        public PicassoCallback(ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        public void onSuccess() {
            holder = null;
        }

        @Override
        public void onError() {
            loadDefaultPicture(holder);
            holder = null;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout layout;
        TextView title;
        TextView description;
        TextView name;
        TextView titulo;
        ImageView picture;
        ImageView icon;
        ImageView background;
        public ViewHolder(View itemView) {
            super(itemView);
            layout = (RelativeLayout) itemView.findViewById(R.id.layout);
            background = (ImageView) itemView.findViewById(R.id.background);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            picture = (ImageView) itemView.findViewById(R.id.picture);
            name = (TextView) itemView.findViewById(R.id.name);
            titulo = (TextView) itemView.findViewById(R.id.titulo);
            description = (TextView) itemView.findViewById(R.id.description);
            title = (TextView) itemView.findViewById(R.id.title);
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if( mOnCoachClickListener != null ){
                        mOnCoachClickListener.OnCoachClickListener(mCoachs.get(getAdapterPosition()));
                    }
                }
            });*/
        }
    }

    /*
    public interface OnCoachClickListener{
        void onCoachClick(Fence fence);
    } */

}
