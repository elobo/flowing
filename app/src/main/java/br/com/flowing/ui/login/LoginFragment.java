package br.com.flowing.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

import br.com.flowing.R;
import br.com.flowing.model.User;
import br.com.flowing.net.CustomCallback;
import br.com.flowing.net.RestServices;
import br.com.flowing.ui.MainActivity;
import br.com.flowing.util.UserPreferences;
import br.com.flowing.util.Validator;
import retrofit.client.Response;
import retrofit.mime.TypedOutput;

/**
 * Created by edgard on 23/05/16.
 */
public class LoginFragment extends Fragment{

    private static final String GRANT_TYPE = "password";
    private EditText userNameEdit, passwordEdit;
    private Button loginBtn;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        inflateViews(view);
        configViews();
        return view;
    }

    private void inflateViews(View view) {
        userNameEdit = (EditText) view.findViewById(R.id.userNameEdit);
        passwordEdit = (EditText) view.findViewById(R.id.passwordEdit);
        loginBtn = (Button) view.findViewById(R.id.loginBtn);
    }

    private void configViews(){
        //userNameEdit.setText("eduardo.guilarducci@flowing.com.br");
        //passwordEdit.setText("123456");
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validator())
                    login(getUserName(), getPassword());
            }
        });
    }

    private boolean validator(){
        if (!Validator.field(userNameEdit.getText().toString(), Validator.EMAIL_PATTERN)){
            invalidField(userNameEdit, "Informe um email válido.");
            return false;
        }
        return true;
    }

    private void invalidField(EditText field, String message){
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        field.requestFocus();
    }

    private void login(String userName, String password){
        RestServices.getUserServices(getActivity(), null)
                .login(GRANT_TYPE, userName, password, new CustomCallback<User>(getActivity()) {
                    @Override
                    public void onError(ErrorResponse response) {
                        Toast.makeText(getActivity(), R.string.login_error_api, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onSuccess(User user, Response response) {
                        UserPreferences.getInstance(getActivity().getApplicationContext()).saveHasUser(true);
                        UserPreferences.getInstance(getActivity().getApplicationContext()).setUserToken(user.getAcessToken());
                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();
                    }
                });
    }


    private String getUserName(){
        return userNameEdit.getText().toString().trim();
    }

    private String getPassword(){
        return passwordEdit.getText().toString().trim();
    }


}
