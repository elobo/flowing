package br.com.flowing.ui.coach;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import br.com.flowing.R;
import br.com.flowing.model.Coach;
import br.com.flowing.net.CustomCallback;
import br.com.flowing.net.RestServices;
import br.com.flowing.util.UserPreferences;
import retrofit.client.Response;

/**
 * Created by edgard on 24/05/16.
 */
public class CoachFragment extends Fragment implements CoachAdapter.OnCoachClickListener{

    private static final String COACHS = "coachs";
    private TextView emptyText;
    private RecyclerView coachRecycler;
    private ProgressBar progress;
    private List<Coach> mCoachs;
    private CoachAdapter coachAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    public static CoachFragment newInstance(){
        return new CoachFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inflateViews(view);
        configView();

        if(savedInstanceState != null ){
            mCoachs = savedInstanceState.getParcelableArrayList(COACHS);
        }

        if(mCoachs == null) {
            showProgress();
            coachs();
        } else {
            showCoachs(mCoachs);
        }
    }

    private void inflateViews(View view) {
        emptyText = (TextView) view.findViewById(R.id.emptyText);
        coachRecycler = (RecyclerView) view.findViewById(R.id.recycler);
        progress = (ProgressBar) view.findViewById(R.id.progress);
    }

    private void configView() {
        coachRecycler.setHasFixedSize(true);
        coachRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mCoachs != null) {
            outState.putParcelableArrayList(COACHS, new ArrayList<Coach>(mCoachs));
        }
    }

    private void showProgress(){
        progress.setVisibility(View.VISIBLE);
    }

    private void showCoachs(List<Coach> coaches) {
        if(isAdded()) {
            mCoachs = coaches;
            coachRecycler.setVisibility(View.VISIBLE);
            emptyText.setVisibility(View.GONE);
            progress.setVisibility(View.GONE);
            updateAdapter(coaches);
        }
    }

    private void showEmpty() {
        if(isAdded()) {
            coachRecycler.setVisibility(View.GONE);
            emptyText.setText(R.string.empty_coach);
            emptyText.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
        }
    }

    private void updateAdapter(List<Coach> coaches) {
        coachAdapter = new CoachAdapter(coaches);
        coachAdapter.setmOnCoachClickListener(this);
        coachRecycler.setAdapter(coachAdapter);
    }

    private void coachs(){
        RestServices.getUserServices(getActivity(), UserPreferences.getInstance(getActivity().getApplicationContext()).userToken())
                .avatars(new CustomCallback<List<Coach>>(getActivity()) {
                    @Override
                    public void onError(ErrorResponse response) {
                        showEmpty();
                    }

                    @Override
                    public void onSuccess(List<Coach> coachs, Response response) {
                        if(coachs != null && coachs.size() > 0)
                            showCoachs(coachs);
                        else
                            showEmpty();
                    }
                });
    }

    @Override
    public void onCoachClick(int position) {
        if(mCoachs.get(position).isSelected())
            mCoachs.get(position).setSelected(false);
        else
            mCoachs.get(position).setSelected(true);
        coachAdapter.notifyItemChanged(position);
    }
}
