package br.com.flowing.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import br.com.flowing.R;
import br.com.flowing.ui.login.LoginActivity;
import br.com.flowing.util.UserPreferences;

public class MainActivity extends AppCompatActivity {

    private MainFragment mainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!UserPreferences.getInstance(getApplicationContext()).hasUser()){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        } else {
            mainFragment = MainFragment.newInstance();
            if (getSupportFragmentManager().findFragmentById(R.id.content) == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.content, mainFragment)
                        .commit();
            } else {
                mainFragment = (MainFragment) getSupportFragmentManager().findFragmentById(R.id.content);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                logout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        UserPreferences.getInstance(getApplicationContext()).clean();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

}
