package br.com.flowing.ui.login;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;

import br.com.flowing.R;
import br.com.flowing.ui.MainFragment;

/**
 * Created by edgard on 23/05/16.
 */
public class LoginActivity extends AppCompatActivity {

    private LoginFragment loginFragment;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if( getSupportFragmentManager().findFragmentById(R.id.fragment_login) == null ){
            loginFragment = LoginFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content, loginFragment)
                    .commit();
        }else{
            loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_login);
        }
    }

    public Fragment getFragment(){
        return loginFragment;
    }


}
