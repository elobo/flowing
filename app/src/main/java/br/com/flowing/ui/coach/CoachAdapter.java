package br.com.flowing.ui.coach;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.flowing.R;
import br.com.flowing.model.Coach;

/**
 * Created by edgard on 24/05/16.
 */
public class CoachAdapter extends RecyclerView.Adapter<CoachAdapter.ViewHolder> {

    private List<Coach> mCoachs;
    private int mDefaultViewSize;
    private OnCoachClickListener mOnCoachClickListener;

    public CoachAdapter(List<Coach> coachs){
        mCoachs = coachs;
    }

    public void setmOnCoachClickListener(OnCoachClickListener onCoachClickListener) {
        mOnCoachClickListener = onCoachClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mDefaultViewSize = (int) parent.getContext().getResources().getDimension(R.dimen.big_view_size);
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_coach, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.check.setChecked(mCoachs.get(position).isSelected());
        holder.name.setText(mCoachs.get(position).getAvatarName());
        holder.bio.setText(mCoachs.get(position).getAvatarBio());
        uploadImage(holder, mCoachs.get(position).getAvatarPicturePath());
    }

    private void uploadImage(ViewHolder holder, String picture){
        try {
            if(picture != null && !picture.isEmpty()) {
                Picasso.with(holder.avatar.getContext())
                        .load(picture)
                        .centerCrop()
                        .resize(mDefaultViewSize, mDefaultViewSize)
                        .into(holder.avatar, new PicassoCallback(holder));
            }
        } catch (Exception e) {
        }
    }


    @Override
    public int getItemCount() {
        return mCoachs.size();
    }

    class PicassoCallback implements com.squareup.picasso.Callback{
        private ViewHolder holder;

        public PicassoCallback(ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        public void onSuccess() {
            holder = null;
        }

        @Override
        public void onError() {
            holder = null;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        CheckBox check;
        ImageView avatar;
        TextView name;
        TextView bio;
        public ViewHolder(View itemView) {
            super(itemView);
            check = (CheckBox) itemView.findViewById(R.id.check);
            avatar = (ImageView) itemView.findViewById(R.id.avatar);
            name = (TextView) itemView.findViewById(R.id.name);
            bio = (TextView) itemView.findViewById(R.id.bio);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if( mOnCoachClickListener != null ){
                        mOnCoachClickListener.onCoachClick(getAdapterPosition());
                    }
                }
            });
        }
    }


    public interface OnCoachClickListener {
        void onCoachClick(int position);
    }

}