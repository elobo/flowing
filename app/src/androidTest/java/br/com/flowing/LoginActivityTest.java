package br.com.flowing;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.flowing.ui.MainFragment;
import br.com.flowing.ui.login.LoginActivity;
import br.com.flowing.ui.login.LoginFragment;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by edgard on 24/05/16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class LoginActivityTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule(LoginActivity.class);

    @Test
    public void testLogin(){
        LoginActivity activity = mActivityRule.getActivity();
        LoginFragment fragment = (LoginFragment) activity.getFragment();
        onView(withId(R.id.userNameEdit)).perform(typeText("eduardo.guilarducci@flowing.com.br"));
        onView(withId(R.id.passwordEdit)).perform(typeText("123456"));
        onView(withId(R.id.loginBtn)).perform(click());
    }

    @Test
    public void testLoginPasswordNotFound(){
        LoginActivity activity = mActivityRule.getActivity();
        LoginFragment fragment = (LoginFragment) activity.getFragment();
        onView(withId(R.id.userNameEdit)).perform(typeText("eduardo.guilarducci@flowing.com.br"));
        onView(withId(R.id.passwordEdit)).perform(typeText("00000"));
        onView(withId(R.id.loginBtn)).perform(click());
    }

    @Test
    public void testLoginEmailNotFound(){
        LoginActivity activity = mActivityRule.getActivity();
        LoginFragment fragment = (LoginFragment) activity.getFragment();
        onView(withId(R.id.userNameEdit)).perform(typeText("teste@email.com"));
        onView(withId(R.id.passwordEdit)).perform(typeText("123456"));
        onView(withId(R.id.loginBtn)).perform(click());
    }

    @Test
    public void testLoginEmailInvalid(){
        LoginActivity activity = mActivityRule.getActivity();
        LoginFragment fragment = (LoginFragment) activity.getFragment();
        onView(withId(R.id.userNameEdit)).perform(typeText("test.email.com"));
        onView(withId(R.id.passwordEdit)).perform(typeText("123456"));
        onView(withId(R.id.loginBtn)).perform(click());
    }
}
